/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#include <iostream>
#include <fstream>
#include <chrono>
#include <boost/regex.hpp>

#include "base/Graph.hpp"
#include "search.hpp"
#include "metrics.hpp"
#include "mst.hpp"
#include "tsp.hpp"
#include "shortest_path.hpp"
//#include "MaximumFlow.hpp"
#include "mcfp.hpp"

int main(int argc, char** argv) {
    std::string input_file = "res/Graph1.txt";
    bool directed = false;
    bool mcp = false;
    // get arguments

    // get arguments
    if (argc > 1) {
        for (int i = 1; i < argc; i++) {
            if (std::strcmp(argv[i], "-f") == 0
                && argc > i + 1)
                input_file = argv[++i];
            else if (std::strcmp(argv[i], "-d") == 0)
                directed = true;
            else if (std::strcmp(argv[i], "-c") == 0)
                mcp = true;

        }
    }

    std::cout << "Load from file " << input_file << " as " << (directed ? " directed " : "undirected ") << " graph" << std::endl;

    Graph g;
    int number_vertices = 0;

    std::ifstream input(input_file);
    if (input.is_open()) {
        std::string line;
        boost::smatch match;

        if (input) {
            std::getline(input, line);
            number_vertices = std::stoi(line);
            g.SetSize(number_vertices);

            std::cout << "#v: " << number_vertices << std::endl;
        }
        // read balances for mcp
        if (mcp) {
            for(int i = 0; i < number_vertices; i++) {
                std::getline(input, line);
                g.vertices_[i]->balance_ = std::stod(line);
            }
        }

        while(std::getline(input, line)) {
            if (boost::regex_match(line, match, boost::regex{R"((\d+)\t(\d+)[\r\n]*)"})) {
                g.AddEdge(std::stoi(match[1]), std::stoi(match[2]), false, directed);
            }
            else if (boost::regex_match(line, match, boost::regex{R"((\d+)\t(\d+)\t([-]?\d+[.]?\d*)[\r\n]*)"})) {
                g.AddEdge(std::stoi(match[1]), std::stoi(match[2]), std::stod(match[3]), directed);
            }
            else if (boost::regex_match(line, match, boost::regex{R"((\d+)\t(\d+)\t([-]?\d+[.]?\d*)\t([-]?\d+[.]?\d*)[\r\n]*)"})) {
                g.AddEdge(std::stoi(match[1]), std::stoi(match[2]), std::stod(match[3]), directed, std::stod(match[4]));
            }

            else {
                std::cout << "Unknown: " << line << std::endl;
            }
        }
    }

    std::cout << "Loading complete" << std::endl;

    auto start = std::chrono::high_resolution_clock::now();
    auto stop = start;
    auto duration = std::chrono::duration_cast<std::chrono::microseconds >(stop - start);

//    ConnectedComponents cc(&g);
//    std::cout << "Components: " << std::endl
//              << cc.GetNumComponents() << " : " << std::endl;
//
//    for (auto i : cc.GetSizeComponents())
//        std::cout << i << ", ";
//    std::cout << std::endl;

//    auto start = std::chrono::high_resolution_clock::now();
//    MSTprim prim(&g);
//    auto stop = std::chrono::high_resolution_clock::now();
//    auto primDuration = std::chrono::duration_cast<std::chrono::microseconds >(stop - start);

//    start = std::chrono::high_resolution_clock::now();
//    MSTkruskal kruskal(&g);
//    stop = std::chrono::high_resolution_clock::now();
//    auto kruskalDuration = std::chrono::duration_cast<std::chrono::microseconds >(stop - start);

//    std::cout << "Prim:    " << prim.Weight() << ", calculated in " << (primDuration.count() / 1000.0) << "ms" << std::endl;
//    std::cout << "Kruskal: " << kruskal.Weight() << ", calculated in " << (kruskalDuration.count() / 1000.0) << "ms" << std::endl;

//    std::cout << prim.MST() << std::endl;



//    std::cout << "TVS with NearestNeighbor:" << std::endl;
//
//    for (auto vs : g.vertices_) {
//        start = std::chrono::high_resolution_clock::now();
//        NearestNeighbor nn(&g, vs.second);
//        stop = std::chrono::high_resolution_clock::now();
//        duration = std::chrono::duration_cast<std::chrono::microseconds >(stop - start);
//
//        if (min == -1 || min > nn.Weight()) {
//            minStart = vs.second->id_;
//            min = nn.Weight();
//            minTime = duration.count() / 1000.0;
//        }
//
//        std::cout << "Start: " << vs.second->id_ << " -> " << nn.Weight() << ", in " << duration.count() / 1000.0 << "ms" << std::endl;
//    }
//
//    std::cout << "Best route: " << min << " with start at " << minStart << ", calculated in " << minTime << "ms" << std::endl;

//    start = std::chrono::high_resolution_clock::now();
//    DoubleTree db(&g, g.vertices_[7]);
//    stop = std::chrono::high_resolution_clock::now();
//    duration = std::chrono::duration_cast<std::chrono::microseconds >(stop - start);
//
//    std::cout << db << std::endl;
//    std::cout << "calculated in " << duration.count() / 1000.0 << "ms" << std::endl;

//    std::cout << "#############################" << std::endl
//              << "TVS with DoubleTree:" << std::endl;
//
//    min = -1;
//    minTime = -1;
//    minStart = -1;
//
//    for (auto vs : g.vertices_) {
//        start = std::chrono::high_resolution_clock::now();
//        DoubleTree doubleTree(&g, vs.second);
//        stop = std::chrono::high_resolution_clock::now();
//        duration = std::chrono::duration_cast<std::chrono::microseconds >(stop - start);
//
//        if (min == -1 || min > doubleTree.Weight()) {
//            minStart = vs.second->id_;
//            min = doubleTree.Weight();
//            minTime = duration.count() / 1000.0;
//        }
//        std::cout << "Start: " << vs.second->id_ << " -> " << doubleTree.Weight() << ", in " << duration.count() / 1000.0 << "ms" << std::endl;
//    }
//
//    std::cout << "Best route: " << min << " with start at " << minStart << ", calculated in " << minTime << "ms" << std::endl;

//    BruteForce bf(&g, g.vertices_[0]);
//    std::cout << bf << std::endl;

//    NearestNeighbor nn(&g, g.vertices_[0]);
//    std::cout << nn << std::endl;
//
//    BranchAndBound bb(g, g.vertices_[0]);
//    std::cout << bb << std::endl;

//    for (auto s : g.vertices_) {
//        BranchAndBound bb(g, s.second);
//        std::cout << bb << std::endl;
//    }

//    Dijkstra dijkstra(g, g.vertices_[0]);
//    dijkstra.PathTo(g.vertices_[1]);
//    std::cout << dijkstra << std::endl;
//
//    MooreBellmanFord mbf(g, g.vertices_[2]);
//    mbf.PathTo(g.vertices_[0]);
//    std::cout << mbf << std::endl;
//
//    auto c = mbf.GetNegativeCycles();
//
//    std::cout << "." << std::endl;

//    std::unordered_map<Vertex*, Vertex*> parents_{};
//    BFS bfs(&g, g.vertices_[0],
//            [](auto v, auto e){ return true; },
//            [&parents_](auto v, auto e){ parents_[e->dst_] = v; } );
//
//    for (auto& ent : parents_) {
//        std::cout << ent.first->id_ << " -> " << ent.second->id_ << std::endl;
//    }


    std::cout << "Cycle-Canceling-Algorithm" << std::endl;
    CCA cca(&g);

    std::cout << std::endl << "Successive-Shortest-Path-Algorithm" << std::endl;
    SSPA sspa(&g);

    return 0;
}

