/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#include <iostream>

#include "Vertex.hpp"
#include "Edge.hpp"

void Vertex::AddEdge(Edge *edge) {
    edges_.emplace(edge->src_ == this ? edge->dst_->id_ : edge->src_->id_, edge);
}

void Vertex::RemoveEdge(int dst) {
    if (edges_.contains(dst))
        edges_.erase(dst);
}

void Vertex::RemoveEdge(Edge *edge) {
    if (edges_.contains(edge->dst_->id_))
        edges_.erase(edge->dst_->id_);
}

std::vector<Edge*> Vertex::GetEdgesOut(EdgeOrder order) {
    std::vector<Edge*> result{};
    for (auto e : edges_) {
        if (!e.second->directed_ || (e.second->directed_ && e.second->src_ == this)){
            result.push_back(e.second);
        }
    }
    switch (order) {
        case kAscending:
            std::sort(result.begin(), result.end(),
                      [](Edge *l, Edge *r) { return l->weight_ < r->weight_; });
            break;
        case kDescending:
            std::sort(result.begin(), result.end(),
                      [](Edge *l, Edge *r) { return l->weight_ > r->weight_; });
            break;
        default:
            break;
    }

    return result;
}

Edge* Vertex::GetEdgeTo(int id) {
    auto e = edges_.find(id);
    if (e == edges_.end())
        return nullptr;
    else
        return e->second;
}

Edge* Vertex::GetEdgeTo(Vertex* dst) {
    auto e = edges_.find(dst->id_);
    if (e == edges_.end())
        return nullptr;
    else
        return e->second;
}

std::ostream &operator<<(std::ostream &os, const Vertex &v) {
    os << v.id_ << " : [ ";
    for (auto e : v.edges_) {
        if (!e.second->directed_ || (e.second && e.second->src_ == &v)){
            os << *e.second << " ";
        }
    }
    os << "]";
    return os;
}
