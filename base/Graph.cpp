/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#include "Graph.hpp"

Graph::Graph() {}

Graph::Graph(size_t number_of_vertices) {
    SetSize(number_of_vertices);
}

Graph::Graph(const Graph& g) {
    for (auto& v : g.vertices_) {
        vertices_.emplace(v.first, new Vertex(v.first));
        vertices_[v.first]->balance_ = v.second->balance_;
    }

    for (auto& v : g.vertices_) {
        for (auto& e : v.second->GetEdgesOut()) {
            AddEdge(e->src_->id_, e->dst_->id_, e->weight_, e->directed_, e->capacity_);
        }
    }
}

void Graph::SetSize(size_t number_of_vertices) {
    vertices_.reserve(number_of_vertices);
    for (int i = 0; i < number_of_vertices; i++)
        vertices_.emplace(i, new Vertex(i));
}

Edge* Graph::AddEdge(Edge* edge) {
    return AddEdge(edge->src_->id_, edge->dst_->id_, edge->weight_, edge->directed_, edge->capacity_);
}

Edge* Graph::AddEdge(int src_id, int dst_id, double weight, bool directed, double capacity) {
    Edge* edge = new Edge(vertices_[src_id], vertices_[dst_id], weight, directed, capacity);
    vertices_[src_id]->AddEdge(edge);

    if (!directed) {
        vertices_[dst_id]->AddEdge(new Edge(vertices_[dst_id], vertices_[src_id], weight, directed, capacity));
    }
    return edge;
}

void Graph::RemoveEdge(Edge *edge) {
    vertices_[edge->src_->id_]->RemoveEdge(edge);
}

std::ostream &operator<<(std::ostream &os, const Graph &g) {
    os << "Vertices:" << std::endl;
    for (auto v : g.vertices_) {
        os << *v.second << std::endl;
    }
    return os;
}