/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#ifndef GRAPH_VERTEX_HPP
#define GRAPH_VERTEX_HPP

#include <vector>
#include <unordered_map>

class Edge;

class Vertex {
public:
    enum EdgeOrder {
        kUnsorted   = 0,
        kAscending  = 1,
        kDescending = 2
    };

    explicit Vertex(int id, double balance = 0.0)
        : id_(id), balance_(balance) {};

    void SetBalance(double balance) {
        balance_ = balance;
    }

    void AddEdge(Edge* edge);

    void RemoveEdge(int dst);
    void RemoveEdge(Edge* edge);

    std::vector<Edge*> GetEdgesOut(EdgeOrder order = kUnsorted);

    Edge* GetEdgeTo(int id);
    Edge* GetEdgeTo(Vertex* dst);

    friend std::ostream &operator<<(std::ostream &os, const Vertex &v);

    int id_{};
    double balance_{};
private:
    std::unordered_map<int, Edge*> edges_{};
};

#endif //GRAPH_VERTEX_HPP
