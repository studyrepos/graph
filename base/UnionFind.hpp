/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#ifndef GRAPH_UNIONFIND_HPP
#define GRAPH_UNIONFIND_HPP

#include <vector>

class UnionFind{
public:
    explicit UnionFind(int size){
        size_ = size;
        id_.resize(size_);
        for (int i = 0; i < size_; i++) {
            id_[i] = i;
        }
    }

    int Size() const {
        return size_;
    }

    bool Connected(int p, int q) {
        return Find(p) == Find(q);
    }

    int Find(int p) {
        return id_[p];
    }

    void Union(int p, int q) {
        int pId = Find(p);
        int qId = Find(q);

        if (pId == qId) return;

        for (int i = 0; i < size_; i++) {
            if (id_[i] == pId)
                id_[i] = qId;
        }
    }


private:
    int size_{};
    std::vector<int> id_{};
};

#endif //GRAPH_UNIONFIND_HPP
