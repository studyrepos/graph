/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#ifndef GRAPH_GRAPH_HPP
#define GRAPH_GRAPH_HPP

#include <map>
#include <unordered_map>
#include <iostream>

#include "Vertex.hpp"
#include "Edge.hpp"

class Graph {
public:
    Graph();
    explicit Graph(size_t number_of_vertices);

    Graph(const Graph& g);

    void SetSize(size_t num_vertices);

    size_t Size() const {
        return vertices_.size();
    }
    Edge* AddEdge(Edge* edge);
    Edge* AddEdge(int src_id, int dst_id, double weight = 0, bool directed = false, double capacity = 0);

    void RemoveEdge(Edge* edge);

    friend std::ostream &operator<<(std::ostream &os, const Graph &g);

    std::unordered_map<int, Vertex*> vertices_{};
};

#endif //GRAPH_GRAPH_HPP
