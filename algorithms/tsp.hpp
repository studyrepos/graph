/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#ifndef GRAPH_TSP_HPP
#define GRAPH_TSP_HPP

#include <unordered_set>
#include <vector>

#include "Graph.hpp"
#include "search.hpp"
#include "mst.hpp"
#include "Node.hpp"

class NearestNeighbor {
public:
    explicit NearestNeighbor(Graph* g, Vertex* start = nullptr) {
        start_ = start == nullptr ? g->vertices_.begin()->second : start;
        Vertex* v = start_;
        Vertex* next;

        std::unordered_set<Vertex*> traversed{};
        traversed.emplace(v);

        while(path_.size() < g->Size() - 1) {
            // iterate over edges of active vertex in ascending order on weight
            // break loop on lightest edge to not traversed vertex
            for (auto e : v->GetEdgesOut(Vertex::kAscending)) {
                // check edge direction and get real next vertex
                next = (v == e->src_ ? e->dst_ : e->src_);
                if (traversed.contains(next))
                    continue;
                traversed.emplace(next);

                // add edge to path
                path_.push_back(e);
                weight_ += e->weight_;
                // go on to next vertex
                v = next;
                break;
            }
        }
        // close hamilton circle with edge back to start vertex
        auto e = v->GetEdgeTo(start);
        if (e != nullptr) {
            path_.push_back(e);
            weight_ += e->weight_;
        }
    }

    double Weight() const {
        return weight_;
    }

    std::vector<Edge*> Path() const {
        return path_;
    }

    friend std::ostream &operator<<(std::ostream &os, const NearestNeighbor &nn) {
        os << "TSP with NearestNeighbor, started at " << nn.start_->id_ << " with weight " << nn.weight_ << ":" << std::endl;
        for (auto e : nn.Path()) {
            os << " -> " << *e << std::endl;
        }
        return os;
    }

private:
    Vertex* start_{};
    double weight_ = 0.0;
    std::vector<Edge*> path_{};
};

class DoubleTree {
public:
    explicit DoubleTree(Graph* g, Vertex* start = nullptr) {
        start_ = start == nullptr ? g->vertices_.begin()->second : start;
        std::unordered_set<Vertex*> traversed{};

        // calculate minimal spanning tree
        MSTprim prim(g, start_);
        // traverse MST with DFS
        DFS dfs(&prim.MST(), start_);
        // iterate over DFS path in order of traversal
        auto dfs_path = dfs.Path();
        for (auto it = 0; it < dfs_path.size(); it++) {
            // use every vertex only once
            if (traversed.contains(dfs_path[it]))
                continue;
            traversed.emplace(dfs_path[it]);

            // get edge from active to next vertex on path or back to start
            auto e = g->vertices_[dfs_path[it]->id_]->GetEdgeTo(
                    it + 1 < dfs_path.size() ? dfs_path[it + 1] : dfs_path[0]);
            if (e != nullptr) {
                path_.push_back(e);
                weight_ += e->weight_;
            }
        }
    }

    double Weight() const {
        return weight_;
    }

    std::vector<Edge*> Path() const {
        return path_;
    }

    friend std::ostream &operator<<(std::ostream &os, const DoubleTree &db) {
        os << "TSP with DoubleTree, started at " << db.start_->id_ << " for weight " << db.Weight() << " :"  << std::endl;
        for (auto& e : db.Path()) {
            os << " -> " << *e << std::endl;
        }
        return os;
    }

private:
    Vertex* start_{};
    double weight_ = 0.0;
    std::vector<Edge*> path_{};
};

class BruteForce{
public:
    explicit BruteForce(Graph* g, Vertex* start = nullptr) {
        start_ = start == nullptr ? g->vertices_.begin()->second : start;

        std::vector<Vertex*> nodes;
        for (auto& v : g->vertices_)
            nodes.push_back(v.second);

        std::sort(nodes.begin(), nodes.end(),
                  [](const Vertex* l, const Vertex* r) { return l->id_ < r->id_; } );

        int i = 0;
        while(std::next_permutation(std::begin(nodes), std::end(nodes),
                [](const Vertex* l, const Vertex* r) { return l->id_ < r->id_; } )) {

            double path_weight = 0;
            std::vector<Edge*> path;
            for (auto it = 0; it < nodes.size(); it++) {
                // get edge from active to next vertex on path or back to start
                auto e = nodes[it]->GetEdgeTo(
                        it + 1 < nodes.size() ? nodes[it + 1] : nodes[0]);
                if (e != nullptr) {
                    path_weight += e->weight_;
                    path.push_back(e);
                }
            }
            if (path_weight < weight_) {
                weight_ = path_weight;
                path_ = path;
                start_ = nodes[0];
            }
            ++i;
        }
        std::cout << "Checked " << i << " possible circles" << std::endl;
    }

    double Weight() const {
        return weight_;
    }

    std::vector<Edge*> Path() const {
        return path_;
    }

    friend std::ostream &operator<<(std::ostream &os, const BruteForce &obj) {
        os << "TSP with BruteForce, started at " << obj.start_->id_ << " for weight " << obj.Weight() << " :" << std::endl;
        for (auto& e : obj.Path()) {
            os << " -> " << *e << std::endl;
        }
        return os;
    }

private:
    Vertex* start_{};
    double weight_ = INFINITY;
    std::vector<Edge*> path_{};
};

class BranchAndBound{
public:
    explicit BranchAndBound(Graph& g, Vertex* start = nullptr) {
        start_ = start == nullptr ? g.vertices_.begin()->second : start;

        NearestNeighbor nn(&g, start_);
        double bound = nn.Weight();

        // first node
        queue_.emplace(new Node(g, start_));

        while(!queue_.empty()) {
            // get next node
            auto node = queue_.top();
            queue_.pop();

            // traverse options
            for (auto e: node->vertex_->GetEdgesOut()) {
                auto dst = node->vertex_ == e->src_ ? e->dst_ : e->src_;
                if (node->traversed_.contains(dst->id_))
                    continue;

                auto child = new Node(node, dst);
                if (child->level == g.Size() - 1) {
                    if ((min_ == nullptr && bound > child->cost_)
                            || (min_ != nullptr && min_->cost_ > child->cost_)) {
                        min_ = child;
                    }
                }
                else if (min_ != nullptr && min_->cost_ < child->cost_) {
                    delete child;
                }
                else {
                    queue_.push(child);
                }
            }
        }

        // calculate the shortest route by reversing predecessors
        if (min_ != nullptr) {
            std::vector<Vertex*> path_vertices;
            path_vertices.push_back(start_);
            path_vertices.push_back(min_->vertex_);
            Node* parent = min_->parent_;
            while (parent != nullptr) {
                path_vertices.push_back(parent->vertex_);
                parent = parent->parent_;

            }

            Edge* e;
            for (auto i = path_vertices.size() - 1; i > 0; i--) {
                e = path_vertices[i]->GetEdgeTo(path_vertices[i-1]);
                weight_ += e->weight_;
                path_.push_back(e);
            }
        }
    }

    double Weight() const {
        return weight_;
    }

    std::vector<Edge*> Path() const {
        return path_;
    }

    friend std::ostream &operator<<(std::ostream &os, const BranchAndBound &obj) {
        os << "TSP with BranchAndBound, started at " << obj.start_->id_ << " for weight " << obj.Weight() << " :" << std::endl;
        for (auto& e : obj.Path()) {
            os << " -> " << *e << std::endl;
        }
        return os;
    }


private:
    Vertex* start_{};
    double weight_ = 0.0;
    std::vector<Edge*> path_{};

    Node* min_ = nullptr;
    std::priority_queue<Node*, std::vector<Node*>,
            decltype([](Node* l, Node* r) { return l->cost_ > r->cost_; })> queue_{};
};

#endif //GRAPH_TSP_HPP
