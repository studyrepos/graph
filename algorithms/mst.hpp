/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#ifndef GRAPH_MST_HPP
#define GRAPH_MST_HPP

#include <vector>
#include <queue>
#include <unordered_set>

#include "Graph.hpp"
#include "UnionFind.hpp"

class MSTprim {
public:
    explicit MSTprim(Graph* g, Vertex* start = nullptr) {
        Edge* e;

        mst_.SetSize(g->Size());

        bool visit_src, visit_dst;
        Visit(start == nullptr ? g->vertices_.begin()->second : start);
        while (!queue_.empty()) {
            e = queue_.top();
            queue_.pop();

            visit_src = !traversed_.contains(e->src_);
            visit_dst = !traversed_.contains(e->dst_);

            if (!visit_src && !visit_dst)
                continue;

            mst_.AddEdge(e);
            weight_ += e->weight_;

            if (visit_src)
                Visit(e->src_);
            if (visit_dst)
                Visit(e->dst_);
        }
    }

    double Weight() const {
        return weight_;
    }

    Graph& MST() {
        return mst_;
    }

private:
    void Visit(Vertex* v) {
        traversed_.emplace(v);
        for(auto& e : v->GetEdgesOut()) {
            if (!traversed_.contains(v == e->src_ ? e->dst_ : e->src_))
                queue_.push(e);
        }
    }

private:
    Graph mst_{};
    double weight_ = 0.0;
    std::unordered_set<Vertex*> traversed_{};
    std::priority_queue<Edge*, std::vector<Edge*>,
            decltype([](Edge* l, Edge* r) { return l->weight_ > r->weight_; })> queue_{};
};

class MSTkruskal {
public:
    explicit MSTkruskal(Graph* g) {

        mst_.SetSize(g->Size());

        for (auto v : g->vertices_) {
            for(auto e : v.second->GetEdgesOut())
                queue_.push(e);
        }

        UnionFind uf((int)g->Size());

        while(!queue_.empty()) {
            Edge* e = queue_.top();
            queue_.pop();
            if (uf.Connected(e->src_->id_, e->dst_->id_))
                continue;

            uf.Union(e->src_->id_, e->dst_->id_);
            mst_.AddEdge(e);
            weight_ += e->weight_;
        }
    }

    double Weight () const {
        return weight_;
    }

    Graph& MST() {
        return mst_;
    }

private:
    Graph mst_{};
    double weight_ = 0.0;
    std::priority_queue<Edge*, std::vector<Edge*>,
            decltype([](Edge* l, Edge* r) { return l->weight_ > r->weight_; })> queue_{};
};


#endif //GRAPH_MST_HPP
