/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#include <functional>
#include <cmath>

#include "Graph.hpp"
#include "search.hpp"

class EdmondKarp {
public:
    EdmondKarp(Graph* g, Vertex* start, Vertex* target) {
        graph_ = g;

        max_flow_ = 0.0;

        // build residual graph as copy of original graph, extended by all reversed edges
        // at start, all reversed edges have capacity = 0
        residual_ = new Graph(*g);

        start_ = residual_->vertices_[start->id_];
        target_ = residual_->vertices_[target->id_];

        std::unordered_map<Vertex*, double> vertex_flow{};
        std::unordered_map<Vertex*, Vertex*> predecessor{};

        for (auto& v : residual_->vertices_) {
            // init predecessors as nullptr
            predecessor.emplace(v.second, nullptr);
            vertex_flow.emplace(v.second, 0);
            for (auto& e : v.second->GetEdgesOut()) {
                // extend residual graph by reversed edges with capacity 0
                residual_->AddEdge(e->dst_->id_, e->src_->id_, 0, true, 0);
            }
        }
        predecessor[start_] = start_;
        vertex_flow[start_] = std::numeric_limits<double>::infinity();

        // set flow for all edges, original and reversed, to 0
        for (auto& v : residual_->vertices_) {
            for (auto& e : v.second->GetEdgesOut()) {
                edge_flow_.emplace(e, 0.0);
            }
        }

        // explore the residual graph until no new connection from start to target can be found
        // in every iteration, capacities of the edges used in the actual path are adjusted
        while(true) {
            BFS search(residual_, start_,
                       [&](auto v, auto e) { return (e->capacity_ - edge_flow_[e]) > 0; },
                       [&](auto v, auto e) {
                           predecessor[e->dst_] = v;
                           vertex_flow[e->dst_] = std::min(vertex_flow[e->src_], e->capacity_ - edge_flow_[e]);
                       } );

            // cancellation point, if there is no connection from start to target
            if (!search.IsConnected(target_))
                break;

            // calculate minimal flow for path
            double path_flow = vertex_flow[target_];

            // adjust flow of path edges, decrement reverse edges, increment original edges
            for (auto v = target_; v != start_; v = predecessor[v]) {
                edge_flow_[predecessor[v]->GetEdgeTo(v)] += path_flow;
                edge_flow_[v->GetEdgeTo(predecessor[v])] -= path_flow;
            }

            // update maximum flow
            max_flow_ += path_flow;
        }
    }

    double GetMaximumFlow() const {
        return max_flow_;
    }

    std::unordered_map<Edge*, double> GetEdgeFlow() const {
        std::unordered_map<Edge*, double> edge_flow{};
        for (auto e : edge_flow_) {
            Edge* g_e = graph_->vertices_[e.first->src_->id_]->GetEdgeTo(graph_->vertices_[e.first->dst_->id_]);
            if (g_e != nullptr)
                edge_flow.emplace(g_e, e.second);
        }
        return edge_flow;
    };

    friend std::ostream &operator<<(std::ostream &os, const EdmondKarp &obj) {
        os << "Maximum flow from " << obj.start_->id_ << " to " << obj.target_->id_
           << " : " << obj.GetMaximumFlow() << std::endl;
        return os;
    }

private:

private:
    Graph* graph_{};
    Graph* residual_{};
    Vertex* start_{};
    Vertex* target_{};

    double max_flow_{};
    std::unordered_map<Edge*, double> edge_flow_{};
};
