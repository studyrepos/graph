/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/


#ifndef GRAPH_MATRIX_HPP
#define GRAPH_MATRIX_HPP

#include <vector>
#include <cmath>
#include <iomanip>
#include "Graph.hpp"

class Matrix{
public:
    explicit Matrix(int size) : size_(size) {
        matrix_.resize(size * size, std::numeric_limits<double>::infinity());
    }

    explicit Matrix(Graph& graph) {
        size_ = graph.Size();
        matrix_.resize(graph.Size() * graph.Size(), std::numeric_limits<double>::infinity());
        for(auto& v : graph.vertices_) {
            for (auto &e: v.second->GetEdgesOut()) {
                matrix_[(e->src_->id_ * size_) + e->dst_->id_] = e->weight_;
                if (!e->directed_)
                    matrix_[(e->dst_->id_ * size_) + e->src_->id_] = e->weight_;
            }
        }
    }

    Matrix(Matrix& matrix) {
        this->size_ = matrix.size_;
        this->matrix_ = matrix.matrix_;
    }

    double Reduce() {
        double reduction_cost = 0.0;

        // reduce rows
        for (int row = 0; row < size_; row++) {
            // get row minimum
            double min_r = std::numeric_limits<double>::infinity();
            for (int col = 0; col < size_; col++) {
                min_r = min_r < matrix_[row * size_ + col] ? min_r : matrix_[row * size_ + col];
            }
            if (min_r == 0.0 || min_r == std::numeric_limits<double>::infinity())
                continue;
            reduction_cost += min_r;
            // reduce row, if minimum greater zero
            for (int col = 0; col < size_; col++) {
                matrix_[row * size_ + col] = matrix_[row * size_ + col] - min_r;
            }
        }

        // reduce columns
        for (int col = 0; col < size_; col++) {
            // get column minimum
            double min_c = std::numeric_limits<double>::infinity();
            for (int row = 0; row < size_; row++) {
                min_c = min_c < matrix_[col + size_ * row] ? min_c : matrix_[col + size_ * row];
            }
            if (min_c == 0.0 || min_c == std::numeric_limits<double>::infinity())
                continue;
            reduction_cost += min_c;
            // reduce column, if minimum greater zero
            for (int row = 0; row < size_; row++) {
                matrix_[col + size_ * row] = matrix_[col + size_ * row] - min_c;
            }
        }
        return reduction_cost;
    }

    double Reduce(int row, int col) {
        // inhibit target row and col
        for (int c = 0; c < size_; c++) {
            matrix_[row * size_ + c] = std::numeric_limits<double>::infinity();
        }
        for (int r = 0; r < size_; r++) {
            matrix_[col + size_ * r] = std::numeric_limits<double>::infinity();
        }
        matrix_[col * size_ + row] = std::numeric_limits<double>::infinity();

        return Reduce();
    }

    friend std::ostream &operator<<(std::ostream &os, const Matrix &m) {
        os << "Matrix " << m.size_ << "x" << m.size_ << ":" << std::endl;
        for (int r = 0; r < m.size_; r++) {
            for (int c = 0; c < m.size_; c++) {
                os << std::setw(6) << std::setprecision(4) << m.matrix_[r * m.size_ + c];
            }
            os << std::endl;
        }
        return os;
    }

//private:
    size_t size_{};
    std::vector<double> matrix_{};

};

#endif //GRAPH_MATRIX_HPP
