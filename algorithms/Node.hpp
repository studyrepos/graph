/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#ifndef GRAPH_NODE_HPP
#define GRAPH_NODE_HPP

#include <unordered_set>


class Node{
public:
    Node(Graph& graph, Vertex* start) {
        vertex_ = start;
        parent_ = nullptr;
        cost_ = 0.0;
        traversed_.emplace(vertex_->id_);
    }

    Node(Node* parent, Vertex* vertex) : traversed_(parent->traversed_) {
        vertex_ = vertex;
        parent_ = parent;
        cost_ = parent->cost_
                + parent_->vertex_->GetEdgeTo(vertex_)->weight_;
        level = parent->level + 1;
        traversed_.emplace(vertex_->id_);
    }

    friend std::ostream &operator<<(std::ostream &os, const Node &o) {
        os << "Node for Vertex " << o.vertex_->id_ << " with cost " << o.cost_ << std::endl;
        return os;
    }

//private:
    double cost_{};
    int level = 0;
    Vertex* vertex_{};
    Node* parent_{};
    std::unordered_set<int> traversed_{};
};

#endif //GRAPH_NODE_HPP
