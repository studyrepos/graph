/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#ifndef GRAPH_SHORTEST_PATH_HPP
#define GRAPH_SHORTEST_PATH_HPP

#include <queue>
#include <cmath>

#include "Graph.hpp"

class Dijkstra {
public:
    explicit Dijkstra(Graph& g, Vertex* start = nullptr) {
        start_ = start == nullptr ? g.vertices_.begin()->second : start;

        for (auto& v : g.vertices_) {
            distance_.emplace(v.second, std::numeric_limits<double>::infinity());
            predecessor.emplace(v.second, nullptr);
            unvisited_.push_back(v.second);
        }

        distance_[start_] = 0;
        predecessor[start_] = start_;

        std::sort(unvisited_.begin(), unvisited_.end(), [this](Vertex* l, Vertex* r) {
            return distance_[l] > distance_[r];
        });

        while (!unvisited_.empty()) {
            auto v = unvisited_.back();

            if (distance_[v] == std::numeric_limits<double>::infinity()) {
                break;
            }

            for (auto e : v->GetEdgesOut()) {
                if (e->weight_ < 0) {
                    valid_graph_ = false;
                    return;
                }
                auto src = e->src_ == v ? e->src_ : e->dst_;
                auto dst = e->src_ == v ? e->dst_ : e->src_;
                if (distance_[dst] > distance_[src] + e->weight_) {
                    distance_[dst] = distance_[src] + e->weight_;
                    predecessor[dst] = src;
                }
            }
            unvisited_.pop_back();
            std::sort(unvisited_.begin(), unvisited_.end(), [this](Vertex* l, Vertex* r) {
                return distance_[l] > distance_[r];
            });
        }
    }

    double Weight() const {
        return weight_;
    }

    std::pair<double, std::vector<Edge*>> PathTo(Vertex* target) {
        target_ = target;
        double weight = 0.0;
        std::vector<Edge*> path;
        if (predecessor[target] != nullptr) {
            std::vector<Vertex*> path_vertices;
            path_vertices.push_back(target);
            Vertex* parent = predecessor[target];
            while (parent != predecessor[parent]) {
                path_vertices.push_back(parent);
                parent = predecessor[parent];
            }

            Edge* e;
            for (auto i = path_vertices.size() - 1; i > 0; i--) {
                e = path_vertices[i]->GetEdgeTo(path_vertices[i-1]);
                weight += e->weight_;
                path.push_back(e);
            }
            weight_ = weight;
            path_ = path;
        }

        return std::make_pair(weight, path);
    }

    std::vector<Edge*> Path() const {
        return path_;
    }

    friend std::ostream &operator<<(std::ostream &os, const Dijkstra &obj) {
        if (!obj.valid_graph_) {
            os << "Dijkstra - Shortest path not valid, graph has edges with negative weight!" << std::endl;
        }
        else {
            os << "Dijkstra - Shortest path from " << obj.start_->id_ << " to " << obj.target_->id_
               << " for weight " << obj.Weight() << " :" << std::endl;
            for (auto &e: obj.Path()) {
                os << " -> " << *e << std::endl;
            }
        }
        return os;
    }

private:
    Vertex* start_{};
    Vertex* target_{};
    double weight_{};

    bool valid_graph_ = true;

    std::vector<Vertex*> unvisited_{};
    std::unordered_map<Vertex*, double> distance_{};
    std::unordered_map<Vertex*, Vertex*> predecessor{};

    std::vector<Edge*> path_{};
};

class MooreBellmanFord {
public:
    explicit MooreBellmanFord(Graph& g, Vertex* start = nullptr, bool check_capacity = false) {
        start_ = start == nullptr ? g.vertices_.begin()->second : start;

        for (auto& v : g.vertices_) {
            // collect edges
            for (auto &e: v.second->GetEdgesOut()) {
                if (check_capacity and e->capacity_ <= 0)
                    continue;
                edges_.push_back(e);
            }
            // init distances and predecessors
            distance_.emplace(v.second, std::numeric_limits<double>::infinity());
            predecessor_.emplace(v.second, nullptr);
        }
        // init entries for start vertex
        distance_[start_] = 0;
        predecessor_[start_] = nullptr;

        // sort the edges, just because...
        std::sort(edges_.begin(), edges_.end(), [](Edge* l, Edge* r) {
            return l->weight_ > r->weight_;
        });

        // run the loop
        for (int i = 0; i < g.Size(); i++) {
            changed_.clear();
            for (auto& e : edges_) {
                if (distance_[e->dst_] > distance_[e->src_] + e->weight_) {
                    distance_[e->dst_] = distance_[e->src_] + e->weight_;
                    predecessor_[e->dst_] = e->src_;
                    changed_.push_back(e->dst_);
                }
            }
            if (changed_.empty())
                break;
        }
        tree_valid = changed_.empty();
    }

    double Weight() const {
        return weight_;
    }

    std::pair<double, std::vector<Edge*>> PathTo(Vertex* target) {
        target_ = target;
        double weight = 0.0;
        std::vector<Edge*> path;

        if (tree_valid && predecessor_[target] != nullptr) {
            std::vector<Vertex*> path_vertices;
            path_vertices.push_back(target);
            Vertex* parent = predecessor_[target];
            while (parent != nullptr) {
                path_vertices.push_back(parent);
                parent = predecessor_[parent];
            }

            Edge* e;
            for (auto i = path_vertices.size() - 1; i > 0; i--) {
                e = path_vertices[i]->GetEdgeTo(path_vertices[i-1]);
                weight += e->weight_;
                path.push_back(e);
            }
            weight_ = weight;
            path_ = path;
        }
        return std::make_pair(weight, path);
    }

    std::unordered_map<Vertex*, std::vector<Edge*>> GetNegativeCycles() {
        std::unordered_map<Vertex*, std::vector<Edge*>> cycles{};

        if (changed_.empty())
            return cycles;

        for (auto& v : changed_) {
            std::unordered_set<Vertex*> visited {v};
            std::vector<Vertex*> path_vertices{v};
            Vertex* pre = predecessor_[v];
            while(pre != v) {
                if (visited.contains(pre) or cycles.contains(pre))
                    break;
                visited.emplace(pre);
                path_vertices.push_back(pre);
                pre = predecessor_[pre];
            }
            // found a cycle
            if (pre == v) {
                path_vertices.push_back(pre);
                std::vector<Edge*> path{};
                Edge* e;
                for (auto i = path_vertices.size() - 1; i > 0; i--) {
                    e = path_vertices[i]->GetEdgeTo(path_vertices[i-1]);
                    path.push_back(e);
                }
                cycles.emplace(v, path);
            }
        }
        return cycles;
    }

    std::vector<Edge*> Path() const {
        return path_;
    }

    friend std::ostream &operator<<(std::ostream &os, const MooreBellmanFord &obj) {
        if (obj.path_.empty()) {
            os << "MooreBellmanFord - Shortest path from " << obj.start_->id_ << " to " << obj.target_->id_
               << " not possible -> Negative cycle!" << std::endl;
        }
        else {
            os << "MooreBellmanFord - Shortest path from " << obj.start_->id_ << " to " << obj.target_->id_
               << " for weight " << obj.Weight() << " :" << std::endl;
            for (auto &e: obj.Path()) {
                os << " -> " << *e << std::endl;
            }
        }
        return os;
    }

private:
    Vertex* start_{};
    Vertex* target_{};
    double weight_{};

    std::vector<Edge*> edges_{};
    std::unordered_map<Vertex*, double> distance_{};
    std::unordered_map<Vertex*, Vertex*> predecessor_{};
    std::vector<Vertex*> changed_{};

    std::vector<Edge*> path_{};
    bool tree_valid = false;
};

#endif //GRAPH_SHORTEST_PATH_HPP
