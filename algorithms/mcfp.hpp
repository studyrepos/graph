/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#ifndef GRAPH_MCFP_HPP
#define GRAPH_MCFP_HPP

#include <cmath>

#include "Graph.hpp"
#include "shortest_path.hpp"
#include "MaximumFlow.hpp"

class CCA {
public:
    explicit CCA(Graph* g) {
        graph_ = g;

        max_flow_ = 0.0;
        min_cost_ = 0.0;

        std::unordered_map<Vertex*, double> vertex_flow{};
        std::unordered_map<Vertex*, Vertex*> predecessor{};

        // INIT

        // copy graph
        residual_ = new Graph(*g);

        // add super source (id -1) and sink (id -2) to residual graph
        residual_->vertices_.emplace(-1, new Vertex(-1));
        residual_->vertices_.emplace(-2, new Vertex(-2));

        // connect super source and sink to graph vertices
        for (auto& v : residual_->vertices_) {
            // skip if super vertex
            if (v.first < 0)
                continue;

            // set balances for super-source and sink
            if (v.second->balance_ > 0.0) {
                // add edge from source to vertex
                residual_->AddEdge(-1, v.first, 0, true, v.second->balance_);
                // increment source balance
                residual_->vertices_[-1]->balance_ += v.second->balance_;
                // set vertex balance to zero
                v.second->balance_ = 0;
            }
            else if (v.second->balance_ < 0.0) {
                // add edge from vertex to sink
                residual_->AddEdge(v.first, -2, 0, true, (-1) * v.second->balance_);
                // increment sink balance
                residual_->vertices_[-2]->balance_ += v.second->balance_;
                // set vertex balance to zero
                v.second->balance_ = 0;
            }
        } // end super source/sink

        start_ = residual_->vertices_[-1];
        target_ = residual_->vertices_[-2];

        // calculate initial b-flow with EdmondKarp
        EdmondKarp ed(residual_, start_, target_);
        edge_flow_ = ed.GetEdgeFlow();
        max_flow_ = ed.GetMaximumFlow();

//        std::cout << "MaxFlow: " << ed.GetMaximumFlow() << std::endl;
//        for (auto& e : edge_flow_)  {
//            std::cout << e.first->src_->id_ << " -> " << e.first->dst_->id_ << " : "
//                      << e.second << " / " << e.first->capacity_ << " | " << e.first->weight_
//                      << std::endl;
//        }

        if (max_flow_ != std::abs(residual_->vertices_[-2]->balance_)) {
            std::cout << "Invalid b-flow, no solution" << std::endl;
            return;
        }
        std::cout << "Valid solution possible" << std::endl;

        /**
         * LOOP-START
         */

        // prepare reverse edges
        for (auto& v: residual_->vertices_) {
            for (auto &e: v.second->GetEdgesOut()) {
                residual_edge_map_[e] = nullptr;
            }
        }
        for (auto &e: residual_edge_map_) {
            residual_edge_map_[e.first] = residual_->AddEdge(
                    e.first->dst_->id_, e.first->src_->id_, (-1) * e.first->weight_, true, 0);
        }
        // end prepare

        while(true) {
            // calculate residual graph
            for (auto& v: residual_->vertices_) {
                for (auto& e: v.second->GetEdgesOut()) {
                    // skip reverse edges, will be updated after original edges
                    if (!residual_edge_map_.contains(e)) {
                        continue;
                    }

                    double f_cap_orig;
                    if (e->src_->id_ == -1) {
                        f_cap_orig = graph_->vertices_[e->dst_->id_]->balance_;
                    }
                    else if (e->dst_->id_ == -2) {
                        f_cap_orig = graph_->vertices_[e->src_->id_]->balance_ * (-1);
                    }
                    else {
                        f_cap_orig = graph_->vertices_[e->src_->id_]->GetEdgeTo(graph_->vertices_[e->dst_->id_])->capacity_;
                    }

                    // update capacities of original edges
                    e->capacity_ = f_cap_orig - edge_flow_[e];

                    // add, edit or remove reverse edges
                    if (edge_flow_[e] > 0.0) {
                        residual_edge_map_[e]->capacity_ = edge_flow_[e];
                    }
                    else  {
                        residual_edge_map_[e]->capacity_ = 0;
                    }
                }
            } // end calculate residual graph

//            std::cout << "Residual " << run << ":" << std::endl;
//            for (auto v : residual_->vertices_) {
//                for (auto e : v.second->GetEdgesOut())
//                    std::cout << e->src_->id_ << " -> " << e->dst_->id_
//                              << " : " << e->capacity_  << " | " << e->weight_ << std::endl;
//            }

            // calculate negative cycle, if possible
            // calculate distances from target = super sink
            // if a valid flow can be calculated, there will be reverse edges with capacity > 0 from sink to vertices
            // in the graph. if started at source, this might not be the case for maximal flow
            MooreBellmanFord mbf(*residual_, target_, true);

            auto cycles = mbf.GetNegativeCycles();
            // cancellation point, if no negative cycles found
            if (cycles.empty())
                break;

            // adjust b-flow in first negative cycle
            double min_cycle_capacity = std::numeric_limits<double>::infinity();
            for (auto& e : cycles.begin()->second) {
                min_cycle_capacity = std::min(min_cycle_capacity, e->capacity_);
            }
            for (auto& e : cycles.begin()->second) {
                if (residual_edge_map_.contains(e)) {
                    edge_flow_[e] += min_cycle_capacity;
                }
                else {
                    edge_flow_[residual_->vertices_[e->dst_->id_]->GetEdgeTo(e->src_->id_)] -= min_cycle_capacity;
                }
            } // end adjust b-flow
        } // end loop

//        std::cout << "Final" << std::endl;
//        for (auto v : residual_->vertices_) {
//            for (auto e : v.second->GetEdgesOut())
//                std::cout << e->src_->id_ << " -> " << e->dst_->id_
//                          << " : " << edge_flow_[e] << "/" << e->capacity_  << " | " << e->weight_ << std::endl;
//        }

        // calculate cost
        for (auto& e : edge_flow_) {
            min_cost_ += e.second * e.first->weight_;
        }

        std::cout << "Flow: " << max_flow_ << std::endl
                  << "Cost: " << min_cost_ << std::endl;
    }

private:
    Graph* graph_{};
    Graph* residual_{};
    Vertex* start_{};
    Vertex* target_{};

    double max_flow_{};
    double min_cost_{};
    std::unordered_map<Edge*, double> edge_flow_{};
    std::unordered_map<Edge*, Edge*> residual_edge_map_{};
};


class SSPA {
public:
    explicit SSPA(Graph* g) {
        graph_ = g;

        max_flow_ = 0.0;
        min_cost_ = 0.0;

        residual_ = new Graph(*g);

        for (auto& v : residual_->vertices_) {
            // prepare balance map
            balance_map_.emplace(v.second, 0.0);
            // set initial flow for all edges
            for (auto& e : v.second->GetEdgesOut()) {
                // f(e) = 0    if c(e) >= 0
                //        u(e) if c(e) <  0
                edge_flow_.emplace(e, e->weight_ >= 0 ? 0 : e->capacity_);
            }
        }

        // recalculate new balance map for initial flow
        for (auto& e : edge_flow_) {
            balance_map_[e.first->src_] += e.second;
            balance_map_[e.first->dst_] -= e.second;
        }

        // prepare reverse edges
        for (auto& v: residual_->vertices_) {
            for (auto &e: v.second->GetEdgesOut()) {
                residual_edge_map_[e] = nullptr;
            }
        }
        for (auto &e: residual_edge_map_) {
            residual_edge_map_[e.first] = residual_->AddEdge(
                    e.first->dst_->id_, e.first->src_->id_, (-1) * e.first->weight_, true, 0);
        }
        // end prepare

        while(true) {
            // check balance
            bool balanced = true;
            for (auto &v: balance_map_) {
                balanced &= v.second == v.first->balance_;
            }
            if (balanced) {
                break;
            }
            // end balance check

            // calculate residual graph
            for (auto& v: residual_->vertices_) {
                for (auto& e: v.second->GetEdgesOut()) {
                    // skip reverse edges, will be updated after original edges
                    if (!residual_edge_map_.contains(e)) {
                        continue;
                    }

                    // update capacities of original edges
                    double f_cap_orig = graph_->vertices_[e->src_->id_]->GetEdgeTo(graph_->vertices_[e->dst_->id_])->capacity_;
                    e->capacity_ = f_cap_orig - edge_flow_[e];

                    // update reverse edges
                    if (edge_flow_[e] > 0.0) {
                        residual_edge_map_[e]->capacity_ = edge_flow_[e];
                    }
                    else  {
                        residual_edge_map_[e]->capacity_ = 0;
                    }
                }
            } // end calculate residual graph

            // search vertex with b(s) - b'(s) > 0
            start_ = nullptr;
            target_ = nullptr;
            for (auto &v: balance_map_) {
                if (v.first->balance_ - v.second > 0) {
                    start_ = v.first;
                    break;
                }
            }

            if (start_ == nullptr) {
                std::cout << "No more pseudo sources -> no valid b-flow" << std::endl;
                return;
            }

            MooreBellmanFord mbf(*residual_, start_, true);

            for (auto& v : balance_map_) {
                if (v.first->balance_ - v.second < 0) {
                    target_ = v.first;
                    auto path = mbf.PathTo(v.first);
                    if (path.second.empty()) {
                        std::cout << "No way from source to sink -> network not large enough" << std::endl;
                        return;
                    }

                    double flow_delta = std::numeric_limits<double>::infinity();
                    // get residual capacity of path
                    for (auto& e : path.second) {
                        flow_delta = std::min(flow_delta, e->capacity_);
                    }
                    flow_delta = std::min(flow_delta, start_->balance_ - balance_map_[start_]);
                    flow_delta = std::min(flow_delta, balance_map_[target_] - target_->balance_);

                    // update flow along path
                    for (auto& e : path.second) {
                        if (residual_edge_map_.contains(e)) {
                            edge_flow_[e] += flow_delta;
                        }
                        else {
                            edge_flow_[residual_->vertices_[e->dst_->id_]->GetEdgeTo(e->src_->id_)] -= flow_delta;
                        }
                    } // end adjust flow

                    // update balance
                    balance_map_[start_] += flow_delta;
                    balance_map_[target_] -= flow_delta;

                    break;
                }
            }
        } // end loop

        // calculate cost
        for (auto& e : edge_flow_) {
            min_cost_ += e.second * e.first->weight_;
        }

        std::cout << "Flow: " << max_flow_ << std::endl
                  << "Cost: " << min_cost_ << std::endl;
    }

private:
    Graph* graph_{};
    Graph* residual_{};
    Vertex* start_{};
    Vertex* target_{};

    double max_flow_{};
    double min_cost_{};
    std::unordered_map<Edge*, double> edge_flow_{};
    std::unordered_map<Edge*, Edge*> residual_edge_map_{};
    std::unordered_map<Vertex*, double> balance_map_{};
};

#endif //GRAPH_MCFP_HPP
