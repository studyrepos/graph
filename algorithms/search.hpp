/*********************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 Fabian Schoettler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ********************************************************************************/

#ifndef GRAPH_SEARCH_HPP
#define GRAPH_SEARCH_HPP

#include <vector>
#include <queue>
#include <map>
#include <unordered_set>
#include <unordered_map>

#include "Graph.hpp"
#include "Vertex.hpp"
#include "Edge.hpp"

class DFS {
public:
    explicit DFS(Graph* graph, Vertex* start = nullptr) {
        graph_ = graph;
        traversed_ = new std::unordered_set<Vertex*>();
        if (start == nullptr)
            Search(graph_->vertices_.begin()->second);
        else
            Search(start);
    }

    DFS(Graph* graph, Vertex* start, std::unordered_set<Vertex*>* traversed) {
        graph_ = graph;
        traversed_ = traversed;
        Search(start);
    }

    bool IsConnected(Vertex* end) {
        return traversed_->contains(end);
    }

    int GetNumTraversed() const {
        return num_traversed;
    }

    std::vector<Vertex*> Path() {
        return path_;
    }

private:
    void Search(Vertex* start) {
        traversed_->emplace(start);
        path_.push_back(start);
        ++num_traversed;

        for (auto e : start->GetEdgesOut()) {
            if (!traversed_->contains(e->dst_))
                Search(e->dst_);
        }
    }

private:
    Graph* graph_{};
    int num_traversed = 0;
    std::unordered_set<Vertex*>* traversed_{};
    std::vector<Vertex*> path_{};
};

class BFS {
public:
    explicit BFS(Graph* graph, Vertex* start = nullptr,
                 const std::function<bool(Vertex*, Edge*)>& condition = nullptr,
                 const std::function<void(Vertex*, Edge*)>& action = nullptr) {
        graph_ = graph;
        traversed_ = new std::unordered_set<Vertex*>();

        if (condition != nullptr)
            condition_ = condition;

        if (action != nullptr)
            action_ = action;

        if (start == nullptr)
            Search(graph_->vertices_.begin()->second);
        else
            Search(start);
    }

    BFS(Graph* graph, Vertex* start, std::unordered_set<Vertex*>* traversed) {
        graph_ = graph;
        traversed_ = traversed;
        Search(start);
    }

    bool IsConnected(Vertex* end) {
        return traversed_->contains(end);
    }

    int GetNumTraversed() const {
        return num_traversed;
    }

private:
    void Search(Vertex* start) {
        std::queue<Vertex*> queue{};
        traversed_->emplace(start);
        queue.push(start);
        ++num_traversed;

        while(!queue.empty()) {
            auto v = queue.front();
            for (auto e : v->GetEdgesOut()) {
                if (!traversed_->contains(e->dst_) && condition_(v, e)) {
                    queue.push(e->dst_);
                    traversed_->emplace(e->dst_);
                    ++num_traversed;
                    action_(v, e);
                }
            }
            queue.pop();
        }
    }

private:
    Graph* graph_{};
    int num_traversed = 0;
    std::unordered_set<Vertex*>* traversed_{};

    std::function<bool(Vertex*, Edge*)> condition_ = [](auto v, auto e){return true;};
    std::function<void(Vertex*, Edge*)> action_ = [](auto v, auto e){};
};

#endif //GRAPH_SEARCH_HPP
